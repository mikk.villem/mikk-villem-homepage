---
title: Home
hide_title: true
sections:
  - section_id: hero
    type: section_hero
    file: section_hero.html
    img: 
    title: I will help your company stand out from the competition. 
    content: |-
      Hi, I'm Mikk. I am an animator, designer and front-end developer. I make animated explainer videos, marketing designs and websites. Browse my portfolio and read my blog. If you get any ideas and want to connect, don't hesitate and make contact. Let's bring your ideas to life!
    actions:
      - label: Let's talk
        url: /contact
        type: button
  - section_id: latest-projects
    type: section_portfolio
    layout_style: mosaic
    title: Recent Work
    subtitle: >-
      What I've been up to.
    projects_number: 6
    view_all_label: View All
    view_all_url: portfolio/index.html
  - section_id: services
    type: section_grid
    title: What Do I Do?
    subtitle: >-
      I read, watch movies and...
    col_number: two
    is_numbered: false
    grid_items:
      - title: Animation
        image: images\grid\Adobe_logos_AE.svg
        image2: images\grid\Adobe_logos_PR.svg
        content: |-
          I make designs move in the 2(.5)D space. Logo animations, explainer videos, looping animations and more. 
      - title: Graphic design
        image: images\grid\Adobe_logos_AI.svg
        image2: images\grid\Adobe_logos_PS.svg
        image3: images\grid\Adobe_logos_ID.svg
        content: |-
          If there's nothing to animate yet, then I can help to get the ball rolling. Logos, posters, banners and all things print. 
      - title: UI design
        image: images\grid\Figma_logo.svg
        content: |-
          Design stuffs for the web and the mobile world. I'm a fan of anything that's clutter free, simple and readable. 
      - title: Front-end development
        image: images\grid\JS_logo.svg
        image2: images\grid\React_logo.svg
        content: |-
          Want to set up a homepage and pals Squarespace and Wix are not your cup of tea, then share your idea, let's make it happen. 
  - section_id: latest-posts
    type: section_posts
    title: Latest from the Blog
    subtitle: >-
      What I've been up to and what I think about it.
    posts_number: 3
    col_number: three
    actions:
    - label: View Blog
      url: blog/index.html
      type: button
layout: advanced
---
