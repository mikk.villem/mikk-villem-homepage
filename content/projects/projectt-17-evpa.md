---
title: Pilates Studio homepage
subtitle: >-
  A simple
date: 2024-06-01
thumb_image:images\projects\evpa\evpa_mobile.png
images:
  - \images\projects\evpa\evpa_desktop.png
  - \images\projects\evpa\evpa_tablet.png
  - \images\projects\evpa\evpa_mobile_2.png
layout: project
---

[https://www.viljandipilates.ee/](https://www.viljandipilates.ee/) - A small personal Pilates Studio needed a fresh look and I provided.

> “A man is as young as his spinal column.“ - Joseph Pilates
