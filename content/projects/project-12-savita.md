---
title: Savita Trio logo and business cards
subtitle: >-
date: 2019-07-17
thumb_image: images\projects\Savita_Business_Card_Mockup_1.jpg
images:
  - images\projects\savita.jpg
layout: project
---

> "A man who stops advertising to save money is like a man who stops a clock to save time." - Henry Ford
