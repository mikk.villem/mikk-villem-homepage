---
title: NotifyApp explainers
subtitle: >-
  2D explainers for a mobile app. NotifyApp allows you to share clutter-free information with unlilmited number of subscribers.
date: 2020-06-11
thumb_image: images\projects\notify_logoreveal.gif
videos:
  - https://www.youtube.com/embed/UHBKT_lxdPw
  - https://www.youtube.com/embed/oaydxJ9Deq8
layout: project
---

> "Hope is not a strategy." - August Walker
