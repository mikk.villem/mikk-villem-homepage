---
title: Metaprint Safety Video
subtitle: >-
  Me: Design, animation and editing live action footage with animations.<br>
  Studio: Hansavideo OÜ
date: 2020-07-15
thumb_image: images\projects\Metaprint_chr_reveal.gif
videos:
  - https://www.youtube.com/embed/gK0-P8AgYw0
layout: project
---

> "The world always seems brighter when you’ve just made something that wasn’t there before." - Neil Gaiman
