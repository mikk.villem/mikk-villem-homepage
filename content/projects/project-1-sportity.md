---
title: Sportity Explainer
subtitle: >-
  Me: Storyboarding / Design / Animation <br>
  Studio: Hansavideo OÜ
date: 2019-05-10
thumb_image: images/projects/Sportity_end.gif
images:
  - images/1_Sportity.jpg
videos:
  - https://www.youtube.com/embed/rwYgB623DlI
  - https://www.youtube.com/embed/C4ORJcl8lRg
layout: project
---

Sportity is a Sport Event Information Distribution App for all kinds of sport events. The app helps organisers to distribute event related information to all the participants quickly and efficiently.

Participants can be divided into different categories so that they receive information only relevant to them. For example, Media will have its own category for their specific needs (media shuttle schedule, press center opening times etc). The organising committee will have its own information available through the app and teams all the information relevant for them. In addition to specific information, all users can have results, starting times etc available through the app.

Participants need to download the app and enter a password which will take them directly to the information according to their profile. The password will be made available from the organiser.

The organiser needs to contact Sportity Ltd (www.sportity.com) for the use of the app.
