---
title: Charity Ball Rally
subtitle: >-
  Brand-, print-, web designs and promotional video.
date: 2019-02-26
thumb_image: images/Palliralli_thumb.jpg
images:
  - images/projects/Palliralli_collage.png
videos:
  - https://www.facebook.com/plugins/video.php?href=https%3A%2F%2Fwww.facebook.com%2F376147016146927%2Fvideos%2F374216539847755%2F&show_text=0&width=560
layout: project
---

> "Designers are meant to be loved, not to be understood." - Margaret Oscar
