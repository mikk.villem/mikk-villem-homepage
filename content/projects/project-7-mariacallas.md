---
title: Maria Callas Marketing Materials
subtitle: >-
  Poster localization, print design, web assets, local trailers
date: 2019-04-30
thumb_image: images/callas.jpg
images:
  - images/projects/callas/callas_collage.jpg
layout: project
---

![Callas ad](images/projects/callas/Callas_solaris.png "Shopping center screen ad")

![Maria Callas calendar front](images/projects/callas/MC_CAlendar.jpg "Maria Callas calendar front")

![Maria Callas calendar back](images/projects/callas/MC_CAlendar_2.jpg "Maria Callas calendar back")

> "Design is not a single object or dimension. Design is messy and complex." - Natasha Jen
