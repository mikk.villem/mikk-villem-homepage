---
title: Children's film days 2019
subtitle: >-
  Me:  Title Design, Print and web asset design <br>
  Client: Cinema Artis
date: 2018-04-30
thumb_image: images/2_lastefest_2020_thumb.jpg
layout: project
---

![Children's film festival designs](images/projects/lff2019.jpg "Children's film festival designs")

> "Every great design begins with an even better story." - Lorinda Mamo
