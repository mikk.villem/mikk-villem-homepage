---
title: Granitop logo animation
subtitle: >-
  Mostly 2D with a light 3D shade thrown in there. Based on client spec.
date: 2022-03-09
thumb_image: images\projects\granitop-min.gif
images:
  - images\projects\granitop_logoscreen.png
  - images\projects\granitop-min.gif
layout: project
---

> "Real change, enduring change, happens one step at a time." - Ruth Bader Ginsburg
