---
title: React Native Coin Flipping Mobile App
subtitle: >-
  Personal project <br>
  UI design, animations, code
date: 2018-12-18
thumb_image: images/Coin_anim_H-02.gif
images:
  - images/coinflipper_thumb.jpg
layout: project
---

https://play.google.com/store/apps/details?id=com.mikkvillem.simplecointoss

https://expo.io/@mikktrix/flat-coin-toss

https://gitlab.com/mikk.villem/simple-coin-toss-heads-or-tails

![Coin Flipping](images/Coin_anim_H-02.gif)

> "Design can be art. Design can be aesthetics. Design is so simple, that's why it is so complicated." - Paul Rand,
