---
title: Tallinn, Tartu and Helsinki cross-border ticketing system
subtitle: >-
  Design and animation for digital ads and banners. Explainer video for bus screens.
date: 2022-03-08
thumb_image: images\projects\pilet2020-square.gif
images:
  - images\projects\Piletisüsteem-min.gif
layout: project
---

Tallinn, Tartu and Helsinki are presumably the first cities in the world to implement a cross-border ticketing system that allows passengers to purchase public transport tickets in all three cities.

The cross-border system allows passengers who travel from Estonia to Helsinki to buy public transport tickets via the Pilet.ee 2020 app. You can buy tickets conveniently using the app without queueing at a kiosk or without an HSL card. Another new feature is the possibility to buy day tickets for Tallinn and Tartu with the HSL card online at https://tallinn.pilet.ee/buy. You can tickets buy entering your HSL card number in the service. You can track your journeys at https://tallinn.pilet.ee/tickets/personalcode

> "It is amazing what you can accomplish if you do not care who gets the credit." - Harry S Truman
