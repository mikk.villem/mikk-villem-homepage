---
title: OnKontakt – vähipatsiendi e-tugi
subtitle: >-
  2D explainer for The North Estonia Regional Hospital
date: 2022-03-09
thumb_image: images\projects\onkontakt-map.gif
videos:
  - https://player.vimeo.com/video/699387890?h=55082150fa
layout: project
---

Koostöös Haigekassa, Regionaalhaigla ja Pärnu Haiglaga algas pilootprojekt “OnKontakt – vähipatsiendi e-tugi”. Projekti viiakse läbi onkoloogia- ja hematoloogiakeskuses.

OnKontakt on vähiravi saavatele patsientidele suunatud kaugteenusmudel, mis võimaldab aktiivravi ajal patsientidel jagada ravist tulenevaid kõrvaltoimeid otse oma ravimeeskonnaga. Kõrvaltoimete raporteerimine toimub Kaiku Health digiplatvormil olevate struktureeritud küsimustike abil.

Patsientide kaasamist alustati 1. aprillil 2022 ja uut tervishoiuteenust osutatakse pilootprojekti raames kuni 30. juuni 2023. Pilootprojekti kaasatakse kuni 500 Regionaalhaiglas süsteemravi saavat rinna- ja soolevähktõvega patsienti.

https://www.onkoloogiakeskus.ee/algas-patsientide-kaasamine-uude-kaugteenusesse-onkontakt

> "In a gentle way, you can shake the world." - Mahatma Gandhi
