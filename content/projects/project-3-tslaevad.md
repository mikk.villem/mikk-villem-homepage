---
title: TS Laevad - Ferry Online Ticketing Explainer
subtitle: >-
  Me:  Animation <br>
  Studio: Eesti Animatsiooni Stuudio / Visible OÜ
date: 2019-04-08
thumb_image: images/projects/TS_intro.gif
videos:
  - https://www.youtube.com/embed/S-B_gTllvrk
  - https://www.youtube.com/embed/NwLyQ6SHIKU
  - https://www.youtube.com/embed/T9ZL-XNRqN0
layout: project
---

> "Design adds value faster than it adds costs." - Joel Spolsky
