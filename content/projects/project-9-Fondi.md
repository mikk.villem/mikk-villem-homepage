---
title: Fondi.ee - Crowdfunding site
subtitle: >-
  Logo design and animation, web landing page design and development
date: 2020-06-10
thumb_image: images\projects\Fondi\Fondi_F_Eased_Loop_2.gif
images:
  - images\projects\Fondi\Fondi_F_Eased_Loop_2.gif
layout: project
---

### Logo design

**Keywords:**

1. Community
2. Support
3. Goodwill
4. Creative
5. Growing
6. Collage

It all starts with a **Moodboard**.
![Fondi icon collage](images/projects/Fondi/Fondi_logo_moodboard.JPG "Fondi logo moodboard")

Let the **sketching** begin.
![Fondi logo sketches](images/projects/Fondi/Fondi_sketching.jpg "Fondi logo sketches")

**Final** logo designs
![Fondi icon collage](images/projects/Fondi/Logo_F_collage_Yellow.jpg "Fondi icon collage")
![Fondi icon and logo](images/projects/Fondi/Logo_Fondi_both_Yellow.jpg "Fondi icon and logo")

### Web design

Built for onboarding and gathering initial pre-launh interest. Responsive web design + development. Landing page, dialogue modal, email subscription collection.

Built with React, Material UI, Firebase.

![Landing page dialogue](images/projects/Fondi/fondi_landing_mockup.jpg "Fondi landing page with open dialogue")
![Fondi landing page](images/projects/Fondi/fondi_landing_dialogue_mockup.jpg "Fondi landing page")

> "Be curious. Read widely. Try new things. I think a lot of what people call intelligence boils down to curiosity." - Aaron Schwartz
