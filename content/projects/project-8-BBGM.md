---
title: Black Bread Gone Mad Home Page
subtitle: >-
  Web design, development
date: 2020-06-09
thumb_image: images\projects\BBGM_thumb.gif
images:
  - images\projects\BBGM_Mockup.jpg
layout: project
---

Responsive web design + development. Landing page, blog section and admin by Netlify CMS. All blog entries written to Markdown for ease of transport. Queries with GraphQL.

Built with JAM Stack - Gatsby JS, React, Netlify CMS, Bootstrap 4.

Gitlab - https://gitlab.com/mikk.villem/BBGM-website

> "If all else fails, [working harder than anyone else] is the greatest competitive advantage of any career." - John C Jay
