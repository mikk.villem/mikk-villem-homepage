---
title: Hansavest Work Exchange Explainer
subtitle: >-
  Me: Treatment, Storyboarding, Design, Animation <br>
  Studio: Hansavideo OÜ
date: 2019-01-05
thumb_image: images/Hansavest_thumb.jpg
videos:
  - https://www.youtube.com/embed/OutYDsMOsHc
layout: project
---

> "You can't use up creativity. The more you use, the more you have." - Maya Angelou
