---
title: TalTech summer training case studies
subtitle: >-
  Me: Storyboarding, design and animation. <br>
  Studio: Hansavideo OÜ
date: 2020-05-17
thumb_image: images\projects\Taltech_intro.png
videos:
  - https://www.youtube.com/embed/T7Xi7RSB47w
  - https://www.youtube.com/embed/OaH_GMqV8bI
layout: project
---

> "A man who stops advertising to save money is like a man who stops a clock to save time." - Henry Ford
