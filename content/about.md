---
title: About Me
subtitle: >-
  This is a short page about me and our work.
image: images/about.png
layout: page
---

I'm a freelance designer and animator currently based in Tallinn, Estonia. I love visual storytelling and have a passion for creating.

>The most exciting phrase to hear in science, the one that heralds new discoveries, is not 'Eureka!', but 'That's funny...' - Isaac Asimov.

I focus on 2D animation and my weapon of choice is Adobe After Effects. I can be recruited to do logo animations, explainer videos, title sequences etc.I've done animations for LHV Bank, SEB Bank, COOP, TS Laevad, Tanker Brewery, Nasdaq OMX Baltic and more.

I'm also developing my Javascript, React and CSS skills to venture into UI design and front-end development. Check out some of my works in the portfolio section.

I’m currently available for freelance work. If you have a project that you want to get started and think you need my help with something, then get in touch.


