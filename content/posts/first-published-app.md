---
title: My first app in Play Store
subtitle: >-
    This is exciting!
excerpt: >-
    On this day my first app made with React Native and Expo got published.
date: 2020-05-16
thumb_image: images\coinflipper_thumb.jpg
image: images\blog\Feature.jpg
layout: post
---


## The Basics of Coin Toss

There is little code snippet that's so simple yet holds so much potential.

```
<!-- JavaScript -->

let result; 

let randomNumber = Math.floor(Math.random() * 2);

if (randomNumber == 0) {
  result = "Heads"
}else{
  result = "Tails"
} 

```

## The MVP

**The app that I made was not what I set out to make**. I set out to make a global leaderboard of the luckiest people in the current week. That would be decided by the current score of coinflips. At some point I realized that if I settle for nothing less, then it would take a heck of a lot more time. So I scaled back. The minimum viable product in this case was a bare bones coin tossing app that keeps track of the owners record and has an animated toss.

The MVP used only one of the screens:
![Figma boards](https://mikkvillem.com/images/blog/Figma_Cointoss.jpg)

## Tools I used
+ [Figma](https://www.figma.com/)
+ [Expo](https://expo.io/)
+ [React Native](https://reactnative.dev/)
+ [After Effects](https://www.adobe.com/products/aftereffects.html?gclid=CjwKCAjwwYP2BRBGEiwAkoBpAvACu7tKB8QG1ctJVJGpHUJTkhDps-4fC0J9pGmlLc3j_MUhXotNbhoC26gQAvD_BwE&sdid=KKQOW&kw=semgeneric&mv=search&ef_id=CjwKCAjwwYP2BRBGEiwAkoBpAvACu7tKB8QG1ctJVJGpHUJTkhDps-4fC0J9pGmlLc3j_MUhXotNbhoC26gQAvD_BwE:G:s&s_kwcid=AL!3085!3!247426734206!e!!g!!adobe%20after%20effects)
+ [Lottie](https://airbnb.design/lottie/)
+ [AdMob](https://admob.google.com/home/)

## The Progression

1. Write flipping and win/lose logic in plain Javascript
2. UI design
3. Set up an Expo project
4. Write the logic in React Native
5. Make the flip animation
6. Implement animation with Lottie
7. Implement adds with AdMob
8. Publish @ Expo
9. Publish @ Play Store

## The Take Away

I'm incredibly happy for having something I made up and live. This was my first project with a multitude of frameworks. Incredible feeling. But that's not it for this little project. Next up, let's find out who's the luckiest person in the world:)

Until then - [download for Android devices](https://play.google.com/store/apps/details?id=com.mikkvillem.simplecointoss)

>If you're walking down the right path and you're willing to keep walking, eventually you'll make progress. <cite>Barack Obama</cite>

![Coin toss](https://mikkvillem.com/images/Coin_anim_H-02.gif)