---
title: Front-end developer work sample test
subtitle: >-
  As the pandemic curbs, I'm looking to fill a developer position.
excerpt: >-
  This sample got me an interview.
date: 2020-07-17
thumb_image: images\blog\taxosearch.jpg
image: images\blog\taxosearch.jpg
layout: post
---

## Career pivot - from project manager to developer

Until recently, my main job has been in project management and sales. And I've been doing animation and design as a freelancer on the side. I've decided to change that and move my main career also towards a skill based direction. I've been learning JavaScript as an ingredient in motion design for years, and about a year ago I decided to focus more on the development side. The lockdown has given me plenty of time to refine my framework(React) knowledge and I have started applying for front-end jobs. It's been tough so far, as the demand for juniors is down and the sector has seen a fair share of layoffs. Nevertheless, here is one example from the process. The job opportunity was for a junior front-end developer position, I passed the first round of based on the CV and the second round was a work sample test. I got a good feedback on the work and got to the final interview round, but missed on the position by a narrow margin. Nevertheless, I wanted to share the sample and the process.

## The assignment - front-end developer work sample

Use the TalTech geology infosystem SARV public API - https://api.geocollections.info. Choose a table and create a webapp which includes at least a general search view and a view for chosen result details.
The tables to choose from: analysis, drillcore, reference, locality, sample, specimen, taxon.
The database structure: https://schema.geocollections.info

Minimum requirements:

- - Separate routes for the search and detailed view

- - At least 5 separate search fields/options

- - Use a CSS framework, like Bootstrap

Optional extras:

- Build with Vue.js
- Filtering results (equals, includes, algab jne.)
- Pagination
- Written summary
- Source code on Gitlab/Github

## My process and submission

1. Refine to technical requirements and brainstorm the initial needed components.
2. Collect moodboard for UI. My favourite tool for it - [Milanote](https://milanote.com/)
3. Create UI mockup and mock brand identity. My favourite tool for it - [Figma](https://figma.com/)
4. Set up the project and code. I went with Vue using Nuxt.js and Bootstrap.
5. Publish the project on [Netlify](https://netlify.com/) and the source code on [Gitlab](https://gitlab.com/mikk.villem/proovikas-taltech)
6. I kept track of the total time for each step. I like to use [Clockify](https://clockify.com/).

![search results](https://mikkvillem.com/images/blog/taxosearch_results.jpg)

## Summary

- The submission - https://taxosearch.netlify.app/
- The source - https://gitlab.com/mikk.villem/proovikas-taltech

All the steps along with getting to know Vue.js as I used it for the first time, the assignment took me around 16 hours to get to a state I was content with. I enjoyed the process and got a good feedback, but missed getting the position in the end. Will keep searching and applying, though.
