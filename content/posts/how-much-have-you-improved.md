---
title: How much have you improved?
subtitle: >-
  Or are you taller than a year before?
excerpt: >-
  “When every one of you is shorter than me, it means that I have been all your heights.”
date: 2022-05-10
thumb_image: images\blog\even-lines.webp
image: images\blog\even-lines.webp
layout: post
---

## The premise - taller than everyone else

[Stephen Merchant](http://www.stephenmerchant.com/) did a great stand up “Hello ladies” and aside from jokes, there was an interesting situation that illustrates the point I want to get you thinking about.

He told the audience how tall he was and asked if there was somebody taller than him. When no hands were raised, he said: “When every one of you is shorter than me, it means that I have been all your heights.”

It’s a good logical conclusion. In some point of his life, he has had the exact height as each audience member. He does not state that he is ultimately better at growing or that none could outgrow him in the future. Just that at that moment in time, he is taller and has grown through all the heights of his audience members.

Now, to make use of this example, I’ll give you a similar situation. See if there are commonalities:

Two players play a computer game. One is better than the other.

That’s all. Or..

There’s an exam. Half of the class gets B, half gets A.

I think both of these situations resemble the Stephen Merchant’s. When you are taller than someone, in most cases, you do not say that you are better than him or that he is inferior. He just (a) does not have it in his genes; or (b) he has not had enough time to grow yet.

## The punchline - all start short

When comparing your skills in a game or in school subjects, the logic stays the same in my mind. When you are better than someone, it just means that others have put less time in those activities or that you have an innate gift for it. I strongly disagree with statements that someone is not capable of doing something. I believe that everything is possible to achieve.

Now I’m not trying to say that everyone can run like Usain or that everyone can understand the numbers like Hawkins. They are the stars. The diamonds. The almost miracles. They are the top ends of human limits at the moment. When you set a goal to start training to run as fast as Bolt and focus on the process not the exact result, I can guarantee that you will reach your own limits. And if you have lower limits than others, then that’s fine. You were meant to. You have done what you can.

This comes up most often in sports. No one should make fun of others or criticise them for being less experienced. It’s very highly likely that the better one has put more time in playing. That’s it. So why not try to give out a little of your knowledge and make a friend? Last time I heard, friends are better than enemies. Be positive, they have the same potential as you do, they just need a little bit more time. You’ll have more fun also. And the thing is, like Stephen, even the tallest have started with being short.

And if you feel that you are not learning as fast as you would like, despite your efforts, then it is very likely that you should not focus on the result. Focus on the process, the evolving. Besides, everyone has something they excel in, and life should be about finding it and focusing on that.

So make the most out of your day and enjoy the growing.
